from typing import Tuple, List
from time import time as sec_since_epoch
import datetime
import numpy as np
from tensorflow import keras
import tensorflow as tf
from tensorflow.keras import models, layers, callbacks, optimizers
from tensorflow.data import Dataset
from tensorflow.keras import regularizers

from lib.inputter import read_data_from_file, TrainData
from lib.boardstate import COL_COUNT, ROW_COUNT, PLAYER_COUNT
from lib.model import prepare_dataset, seed_randomness

DATA_FILE: str = "./data/c4-50k.csv"

# Model setup consts
LEARNING_RATE: float = 0.005
EPOCHS: int = 256
TRAIN_SIZE: float = 0.9
VALIDATION_SIZE: float = 1 - TRAIN_SIZE
REGULISATION: float = 0.001

def build_model() -> keras.Model:
    model = models.Sequential()

    model.add(layers.Input(shape=(COL_COUNT * ROW_COUNT * PLAYER_COUNT)))
    model.add(layers.Dense(COL_COUNT * ROW_COUNT * PLAYER_COUNT * 4, 
        kernel_regularizer=regularizers.l2(REGULISATION),
        activation=keras.activations.relu))
    model.add(layers.Dense(COL_COUNT * ROW_COUNT * PLAYER_COUNT * 2, 
        kernel_regularizer=regularizers.l2(REGULISATION),
        activation=keras.activations.relu))
    model.add(layers.Dense(COL_COUNT * ROW_COUNT * PLAYER_COUNT * 2, 
        kernel_regularizer=regularizers.l2(REGULISATION),
        activation=keras.activations.relu))
    model.add(layers.Dense(COL_COUNT, activation=keras.activations.softmax))
    model.summary()

    model.compile(
        optimizer=optimizers.SGD(learning_rate=LEARNING_RATE),
        loss="categorical_crossentropy",
        metrics=["accuracy"]
    )

    return model


def train_model(model: keras.Model, train_data: Dataset, validate_data: Dataset, out_dir: str):

    tensorboard_callback = callbacks.TensorBoard(out_dir + "/log")
    checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
        out_dir + "/checkpoint")

    model.fit(
        x=train_data,
        epochs=EPOCHS,
        callbacks=[tensorboard_callback, checkpoint_callback],
        validation_data=validate_data,
        shuffle=False,
        use_multiprocessing=True
    )


def split_traindata_list(full_dataset: List[TrainData]) -> Tuple[List[TrainData], List[TrainData]]:
    assert np.isclose(TRAIN_SIZE + VALIDATION_SIZE, 1.0)

    train_size = int(TRAIN_SIZE * len(full_dataset))

    train_dataset = full_dataset[:train_size]
    validation_dataset = full_dataset[train_size:]

    return (train_dataset, validation_dataset)


def get_datasets() -> Tuple[Dataset, Dataset]:
    all_data = read_data_from_file(DATA_FILE)
    train_data, validate_data = split_traindata_list(all_data)

    train_data = prepare_dataset(train_data)
    validate_data = prepare_dataset(validate_data)

    return (train_data, validate_data)


def main():
    seed_randomness()

    out_dir = "models/" + \
        str(datetime.datetime.fromtimestamp(
            sec_since_epoch()).strftime("%Y-%m-%d_%H-%M-%S"))

    train_dataset, validation_dataset = get_datasets()

    model = build_model()

    model.summary()
    input("press enter to continue")

    train_model(model, train_dataset, validation_dataset, out_dir)
    model.save(out_dir + "/model")


main()
