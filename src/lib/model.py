from typing import List, Tuple
from tensorflow.data import Dataset
from tensorflow import keras
import numpy as np
import tensorflow as tf
import random

from lib.inputter import TrainData
from lib.boardstate import BoardState, Colour

PREFETCH_BUFFER_SIZE: int = 512
BATCH_SIZE: int = 100

def seed_randomness():
    # Code consts
    MIHALY_STUDENT_NR: int = 1644219
    JEROEN_STUDENT_NR: int = 1642848
    WARD_STUDENT_NR: int = 1642793
    np.random.seed(MIHALY_STUDENT_NR + WARD_STUDENT_NR + JEROEN_STUDENT_NR)
    tf.random.set_seed(MIHALY_STUDENT_NR + WARD_STUDENT_NR + JEROEN_STUDENT_NR)
    random.seed(MIHALY_STUDENT_NR + WARD_STUDENT_NR + JEROEN_STUDENT_NR)
    print("Seeded random generators")

def extract_traindata(train_data_list: List[TrainData]) -> Tuple[np.array, np.array]:

    data = list(map(lambda train_data: train_data.to_arrays(),
                train_data_list))

    inp = [x for x, _ in data]
    out = [y for _, y in data]

    return (inp, out)

def prepare_dataset(dataset: List[TrainData]) -> Dataset:

    dataset_size = len(dataset)

    (inp, out) = extract_traindata(dataset)

    inp = Dataset.from_tensor_slices(inp)
    out = Dataset.from_tensor_slices(out)

    dataset = Dataset.zip((inp, out)) \
        .shuffle(dataset_size) \
        .batch(BATCH_SIZE, drop_remainder=True) \
        .prefetch(PREFETCH_BUFFER_SIZE)

    return dataset


def next_ai_move(model: keras.Model, board: BoardState, player: Colour) -> Tuple[int, float]:
    board_as_model_input = board.to_numpy_array(player)
    column_probs = model.predict(np.array([board_as_model_input]))
    
    column = int(np.unravel_index(column_probs.argmax(), column_probs.shape)[1])
    probability = column_probs[0][column]

    return (column, probability)
