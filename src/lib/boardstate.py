from __future__ import annotations
from enum import Enum
import typing
import numpy as np
import colorama
from colorama import Style, Fore


class Colour(Enum):
    RED = -1
    YELLOW = 1

    def invert(self) -> Colour:
        if self == Colour.YELLOW:
            return Colour.RED
        else:
            return Colour.YELLOW


PLAYER_COUNT: int = len(list(Colour))
COL_COUNT: int = 7
ROW_COUNT: int = 6
CONNECT_AMOUNT = 4

HORIZONTAL = (1, 0)
VERTICAL = (0, 1)
DIAGONAL = (1, 1)
ANTAGONAL = (-1, 1)
DIRECTIONS = [HORIZONTAL, VERTICAL, DIAGONAL, ANTAGONAL]


def valid_position(col: int, row: int) -> bool:
    return col < COL_COUNT and row < ROW_COUNT and col >= 0 and row >= 0


def to_index(col: int, row: int) -> int:
    return ROW_COUNT * col + row


class BoardState:
    def __init__(self):
        self.red = np.zeros((COL_COUNT * ROW_COUNT,), np.bool)
        self.yellow = np.zeros((COL_COUNT * ROW_COUNT,), np.bool)

    def clone(self) -> BoardState:
        new_board_state = BoardState()
        new_board_state.red = np.copy(self.red)
        new_board_state.yellow = np.copy(self.yellow)

        return new_board_state

    def flip(self):
        # Not sure if this works. If you encouter a bug, start here.
        self.red, self.yellow = self.yellow, self.red

    def get_cell(self, col: int, row: int) -> typing.Union[Colour, None]:
        assert valid_position(col, row)

        index = to_index(col, row)

        if self.red[index]:
            return Colour.RED
        if self.yellow[index]:
            return Colour.YELLOW
        return None

    def set_cell(self, col: int, row: int, player: Colour):
        assert valid_position(col, row)

        index = to_index(col, row)

        if player == Colour.RED:
            self.red[index] = player
            assert not self.yellow[index]
        else:
            self.yellow[index] = player
            assert not self.red[index]

    def get_col_height(self, col: int) -> int:
        for row in range(ROW_COUNT):
            if not self.get_cell(col, row):
                return row
        return ROW_COUNT

    def print_board(self):
        """Prints the board to stdout."""
        for row in reversed(range(ROW_COUNT)):
            print(str(row) + " [", end="")
            for col in range(COL_COUNT):
                cell = self.get_cell(col, row)
                if cell == Colour.RED:
                    print(Style.BRIGHT + Fore.RED +
                          "\u26AB" + Style.RESET_ALL, end="")
                elif cell == Colour.YELLOW:
                    print(Style.BRIGHT + Fore.YELLOW +
                          "\u26AB" + Style.RESET_ALL, end="")
                else:
                    print("  ", end="")
            print("]")

        # Printing indices
        print(" " * ((ROW_COUNT//10)+3), end="")
        print(' '.join(map(str, range(COL_COUNT))))

    def to_numpy_array(self, player: Colour) -> np.array:
        """Returns the current boards in the format, used by Keras and TF."""
        vectorize = np.vectorize(lambda item: 1.0 if item else 0.0, otypes=[np.float32])
        if (player == Colour.RED):
            return vectorize(np.concatenate((self.yellow, self.red)))
        else:
            return vectorize(np.concatenate((self.red, self.yellow)))
        
    def is_move_legal(self, col: int) -> bool:
        col_height = self.get_col_height(col)
        return col_height < ROW_COUNT
    
    def is_full(self) -> bool:
        for i in range(COL_COUNT):
            if (self.is_move_legal(i)):
                return False
        return True

    def try_do_move(self, player: Colour, col: int) -> bool:
        assert isinstance(player, Colour)
        assert isinstance(col, int)

        col_height = self.get_col_height(col)

        if col_height >= ROW_COUNT:
            return False
        self.set_cell(col, col_height, player)
        return True


    def do_move(self, player: Colour, col: int):
        result = self.try_do_move(player, col)
        assert result

    def get_winner(self) -> typing.Union[Colour, None]:
        for col in range(COL_COUNT):
            for row in range(ROW_COUNT):
                for direction in DIRECTIONS:
                    winner = self.check_connection(col, row, direction)
                    if winner is not None:
                        return winner
        return None

    def check_connection(
            self,
            col: int,
            row: int,
            direction: typing.Tuple[int, int]
        ) -> typing.Union[Colour, None]:
        assert valid_position(col, row)

        player = self.get_cell(col, row)

        if player is None:
            return None

        for _ in range(CONNECT_AMOUNT - 1):
            col += direction[0]
            row += direction[1]
            if not valid_position(col, row):
                return None
            if not self.get_cell(col, row) == player:
                return None

        return player


# Required for printing
colorama.init()
