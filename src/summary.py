import argparse
from tensorflow import keras

def get_args() -> str:
    parser = argparse.ArgumentParser(description="Play a model.")
    parser.add_argument('model', type=str,help='the model')
    args = parser.parse_args()
    return args


model_name = get_args().model
model = keras.models.load_model(model_name)
model.summary()
