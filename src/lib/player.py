import random
from tensorflow import keras
from lib.boardstate import Colour, BoardState, COL_COUNT, ROW_COUNT
from lib.model import next_ai_move

class Player:
    def __init__(self, colour: Colour):
        self.colour = colour


class RandomPlayer(Player):
    def __init__(self, colour: Colour):
        super().__init__(colour)

    def next_move(self, board: BoardState) -> int:
        moves = list(range(0, 7))
        random.shuffle(moves)

        for move in moves:
            if board.is_move_legal(move):
                return move

        return moves[0]


class ImprovedRandomPlayer(Player):
    def __init__(self, colour: Colour):
        super().__init__(colour)

    def next_move(self, board: BoardState) -> int:
        legal_moves = list()
        opponent_colour = self.colour.invert()

        for col in range(0, COL_COUNT):
            newboard = board.clone()
            if not newboard.try_do_move(self.colour, col):
                break
            winner = newboard.get_winner()
            if winner == self.colour:
                # print("forced victory")
                return col
            legal_moves.append(col)
        
        
        for col in legal_moves:
            newboard = board.clone()
            if not newboard.try_do_move(opponent_colour, col):
                break
            winner = newboard.get_winner()
            if winner == opponent_colour:
                # print("cockblocked")
                return col
        
        if len(legal_moves) == 0:
            # print("hopeless")
            return random.randint(0, COL_COUNT - 1)
        else:
            # print("random valid move")
            random.shuffle(legal_moves)
            return legal_moves[0]



class HumanPlayer(Player):
    def __init__(self, colour: Colour):
        super().__init__(colour)


    def next_move(self, board: BoardState) -> int:
        column_id = int(input(f"{str(self.colour)} column id: "))
        return column_id


class AIPlayer(Player):
    def __init__(self, colour: Colour, model: keras.Model):
        super().__init__(colour)
        self.model = model


    def next_move(self, board: BoardState) -> int:
        return next_ai_move(self.model, board, self.colour)[0]