from typing import List, Iterator, Dict, Tuple
import numpy as np
from lib.boardstate import Colour, BoardState, ROW_COUNT, COL_COUNT

class DataLine:
    def __init__(self, boardid: int, moveid: int, playerid: int, columnid: int, winner: int):
        self.boardid = boardid
        self.moveid = moveid
        self.player = Colour(playerid)
        self.columnid = columnid
        self.winner = Colour(winner)

    @staticmethod
    def from_str(fromvar: str):
        """Creates a DataLine from a line of csv"""
        words: List[str] = fromvar.split(",")
        assert len(words) == 5
        numbers = list(map(int, words))
        return DataLine(numbers[0], numbers[1], numbers[2], numbers[3], numbers[4])


class TrainData:
    def __init__(self, board_state: BoardState, player: Colour, col: int, winner: Colour):
        self.board_state = board_state
        self.player = player
        self.col = col
        self.winner = winner

    def player_is_winner(self) -> bool:
        return self.player == self.winner

    def _to_training_output(self) -> np.array:
        default_value = 0.0 if self.player_is_winner() else 1.0 / float(COL_COUNT - 1)
        return_value = np.repeat(default_value, COL_COUNT)

        return_value[self.col] = 1.0 if self.player_is_winner() else 0.0
        return return_value

    def to_arrays(self) -> Tuple[np.array, np.array]:
        return (self.board_state.to_numpy_array(self.player), self._to_training_output())


class Game:
    def __init__(self):
        self.winner = None
        self.moves: List[Tuple[Colour, int]] = list()

    def get_move(self, index: int) -> Tuple[Colour, int]:
        """Gets move [index]."""
        assert index < self.length()
        return self.moves[index]

    def get_last_move(self) -> Tuple[Colour, int]:
        """Gets last move."""
        assert not self.is_empty()
        return self.get_move(self.length() - 1)

    def length(self) -> int:
        """Returns the amount of moves in this game."""
        return len(self.moves)

    def is_empty(self) -> bool:
        """Returns whether there are any moves in this game."""
        return self.length() < 1

    def add_move(self, move: DataLine):
        if not self.is_empty():
            assert move.player != self.get_last_move()[0]
            assert move.winner == self.winner
        else:
            self.winner = move.winner
        self.moves.append((move.player, move.columnid))

    def to_train_data(self) -> Iterator[TrainData]:
        """Converts all the moves in this game to TrainData"""
        board_state = BoardState()

        for player, col in self.moves:
            if player == self.winner:
                yield TrainData(board_state.clone(), player, col, self.winner)

            if board_state.get_col_height(col) < ROW_COUNT:
                board_state.do_move(player, col)
            else:
                break


def games_to_train_data(games: Iterator[Game]) -> Iterator[TrainData]:
    for game in games:
        game: Game = game
        yield from game.to_train_data()


def read_games_from_file(filename: str) -> List[Game]:
    with open(filename) as opened_file:
        lines = opened_file.readlines()
        lines = map(lambda line: line.strip(), lines)
        lines = filter(lambda line: len(line) > 0, lines)
        all_moves = list(map(DataLine.from_str, lines))

        games: Dict = dict()

        for move in all_moves:
            game = games.setdefault(move.boardid, Game())
            game.add_move(move)
    return list(games.values())


def read_data_from_file(filename: str) -> List[TrainData]:
    games = read_games_from_file(filename)
    return list(games_to_train_data(games))
