import argparse
from tensorflow import keras
from typing import Tuple
from tensorflow.data import Dataset

from lib.model import prepare_dataset
from lib.inputter import read_data_from_file

DATA_FILE: str = "./data/c4-10k.csv"


def get_args() -> Tuple[str, str]:
    parser = argparse.ArgumentParser(description="test a model.")
    parser.add_argument('model_file', type=str,
                        help='Directory to read model from')
    parser.add_argument('--test-data', type=str,
                        help='Test data', default=DATA_FILE)
    args = parser.parse_args()

    return args.model_file, args.test_data


def test_model(model: keras.Sequential, test_dataset: Dataset):
    #tensorboard_callback = callbacks.TensorBoard(log_dir=out_dir + "/log")

    results = model.evaluate(
        x=test_dataset,
        # callbacks=[tensorboard_callback],
        use_multiprocessing=True,
        return_dict=False
    )
    print("test loss, test acc:", results)


def main():
    model_file, data_file = get_args()

    model = keras.models.load_model(model_file)
    test_dataset = prepare_dataset(read_data_from_file(data_file))

    test_model(model, test_dataset)


main()
