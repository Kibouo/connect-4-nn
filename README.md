# How to play

Run `src/play.py player_type player_type amount_games` to play Connect 4. 

Possible player types are:
- human
- random
- irandom
- path to the model

Use the `-h` flag for more detailed information.

## Examples
`python3 src/play.py models/model_5/model human 1`

`python3 src/play.py irandom models/model_7/model 1`
