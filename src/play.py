from lib.boardstate import BoardState, Colour
from lib.player import HumanPlayer, AIPlayer, RandomPlayer, ImprovedRandomPlayer
import argparse
from tensorflow import keras
from lib.model import seed_randomness
import colorama
from colorama import Style, Fore


def get_args() -> str:
    parser = argparse.ArgumentParser(description="Play a model.")
    parser.add_argument('yellow', type=str,
                        help='\'human\', \'random\', \'irandom\' or the path to the model')
    parser.add_argument('red', type=str,
                        help='\'human\', \'random\', \'irandom\' or the path to the model')
    parser.add_argument('count', type=int, default=1,
                        help='1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, etc.')
    parser.add_argument('--hide', action='store_true',
                        help='Hide the intermediate boards')
    args = parser.parse_args()
    return args


def get_player(arg: str, colour: Colour):
    if arg == "human":
        return HumanPlayer(colour)
    elif arg == "random":
        return RandomPlayer(colour)
    elif arg == "irandom":
        return ImprovedRandomPlayer(colour)
    else:
        return AIPlayer(colour, keras.models.load_model(arg))


def main():
    seed_randomness()
    args = get_args()


    curr_player = get_player(args.yellow, Colour.YELLOW)
    next_player = get_player(args.red, Colour.RED)

    red_wins = 0
    yellow_wins = 0
    draws = 0
    games = 0

    while games < args.count:
        games += 1
        board = BoardState()

        if curr_player.colour == Colour.RED:
            curr_player, next_player = next_player, curr_player
        
        winner = None

        while winner is None and not board.is_full():
            if not args.hide:
                board.print_board()

            if not board.try_do_move(curr_player.colour, curr_player.next_move(board)):
                winner = next_player.colour
                break

            curr_player, next_player = next_player, curr_player
            if winner is None:
                winner = board.get_winner()

        if winner == Colour.RED:
            red_wins += 1
        elif winner == Colour.YELLOW:
            yellow_wins += 1
        else:
            assert board.is_full()
            draws += 1

        board.print_board()
        if winner == Colour.RED:
            print(Style.BRIGHT + Fore.RED +
                          "\u26AB" + Style.RESET_ALL, end="")
        else:
            print(Style.BRIGHT + Fore.YELLOW +
                          "\u26AB" + Style.RESET_ALL, end="")

        print(f"{games}")

    print()
    print(f"{colorama.Fore.YELLOW}{args.yellow}{colorama.Style.RESET_ALL} VS. {colorama.Fore.RED}{args.red}{colorama.Style.RESET_ALL}")
    print(f"Yellow: {yellow_wins} ({yellow_wins/args.count * 100}%)")
    print(f"Red: {red_wins} ({red_wins/args.count * 100}%)")
    print(f"Draws: {draws} ({draws/args.count * 100}%)")

main()
